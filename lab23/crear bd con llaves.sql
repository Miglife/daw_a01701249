CREATE TABLE Clientes_Banca
(
  NoCuenta varchar(5) NOT NULL,
  Nombre varchar(30),
  Saldo numeric(10,2),
  PRIMARY KEY (NoCuenta)
)

CREATE TABLE Tipos_Movimiento
(
  ClaveM varchar(2) NOT NULL,
  Descripcion varchar(30),
    PRIMARY KEY (ClaveM)
)

CREATE TABLE Movimientos
(
  NoCuenta varchar(5) NOT NULL,
  ClaveM varchar(2) NOT NULL,
  Fecha datetime,
  Monto numeric(10,2),
)

  ALTER TABLE Movimientos add 
  foreign key (NoCuenta) references Clientes_Banca(NoCuenta); 
   
   ALTER TABLE Movimientos add 
  foreign key (ClaveM) references Tipos_Movimiento(ClaveM); 


