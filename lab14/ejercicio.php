<?php

function conectar() {
    $mysql = mysqli_connect("localhost","root","","becario");
    $mysql->set_charset("utf8");
    return $mysql;
}

function desconectar($mysql) {
    mysqli_close($mysql);
}

// AQUI PROBANDO Weas

/*
function GetConsultaBoton(){
    
    $db= conectar();
    $nombre = $_POST['nombre'];

    //tiene que recibir el post 
    
    $query="SELECT * FROM becados WHERE nombre = $nombre";

    $registros = $db->query($query);

    
    
         
    $table = '<table class="striped">
        <thead>
          <tr>
              <th>Nombre</th>
              <th>Apellido Paterno</th>
              <th>Apellido Materno</th>
              <th>Status</th>
          </tr>
        </thead>
        <tbody>';
    
    $resultados = mysqli_query($conexion,"SELECT * FROM becados WHERE nombre = $nombre");
     while($consulta = mysqli_fetch_array($resultados))
          {
            	$table .= '
    	<tr>
            <td>'.$consulta["Nombre"].'</td>
            <td>'.$consulta["ApellidoP"].'</td>
            <td>'.$consulta["ApellidoM"].'</td>
            <td>'.$consulta["Status"].'</td>
        </tr>';
      
    }
    // it releases the associated results
    mysqli_free_result($registros);
    
    desconectar($db);
    
    $table .= "</tbody></table>";
    
    return $table;
    
    
    
}
*/





function Gettablasbecarios() {
    $db = conectar();
    
    //Specification of the SQL query
    $query='SELECT * FROM becados';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);
        
    echo "<br><br>";
    echo "Esta tabla representa todos los registros en la tabla becarios";

    
    $table = '<table class="striped">
        <thead>
          <tr>
              <th>Nombre</th>
              <th>Apellido Paterno</th>
              <th>Apellido Materno</th>
              <th>Status</th>
          </tr>
        </thead>
        <tbody>';
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
        
    	$table .= '
    	<tr>
            <td>'.$fila["Nombre"].'</td>
            <td>'.$fila["ApellidoP"].'</td>
            <td>'.$fila["ApellidoM"].'</td>
            <td>'.$fila["Status"].'</td>
        </tr>';
      
    }
    // it releases the associated results
    mysqli_free_result($registros);
    
    desconectar($db);
    
    $table .= "</tbody></table>";
    
    return $table;
}


function GettablaAntonio() {
    $db = conectar();
    
    echo "<br> <br>";
    echo "Esta tabla representa todos los datos del registro buscando 'Antonio' en la base de datos";
    //Specification of the SQL query
    $query='SELECT * FROM becados where nombre = "Antonio " ';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);
    
    
    $table = '<table class="striped">
        <thead>
          <tr>
              <th>Nombre</th>
              <th>Apellido Paterno</th>
              <th>Apellido Materno</th>
              <th>Status</th>
          </tr>
        </thead>
        <tbody>';
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
        
    	$table .= '
    	<tr>
            <td>'.$fila["Nombre"].'</td>
            <td>'.$fila["ApellidoP"].'</td>
            <td>'.$fila["ApellidoM"].'</td>
            <td>'.$fila["Status"].'</td>
        </tr>';
      
    }
    // it releases the associated results
    mysqli_free_result($registros);
    
    desconectar($db);
    
    $table .= "</tbody></table>";
    
    return $table;
}


function GettablaHistoriaA() {
    $db = conectar();
    
    echo "<br> <br>";
    echo "Esta tabla representa todos los datos de historiaacademica en la base de datos";
    //Specification of the SQL query
    $query='SELECT * FROM historiaacademica';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);
    
    
    $table = '<table class="striped">
        <thead>
          <tr>
              <th>Idinfo</th>
              <th>IdBecario</th>
              <th>Escuela</th>
              <th>Grado</th>
              <th>Nivel</th>
              <th>Promedio</th>
              <th>Estatus</th>
              
          </tr>
        </thead>
        <tbody>';
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
        
    	$table .= '
    	<tr>
            <td>'.$fila["Idinfo"].'</td>
            <td>'.$fila["IdBecario"].'</td>
            <td>'.$fila["Escuela"].'</td>
            <td>'.$fila["Grado"].'</td>
            <td>'.$fila["Nivel"].'</td>
            <td>'.$fila["Promedio"].'</td>
            <td>'.$fila["Estatus"].'</td>


        </tr>';
      
    }
    // it releases the associated results
    mysqli_free_result($registros);
    
    desconectar($db);
    
    $table .= "</tbody></table>";
    
    return $table;
}

function GettablaHistoriaATEC() {
    $db = conectar();
    
    echo "<br> <br>";
    echo "Esta tabla representa algunas columnas del registro buscando 'TEC' en la base de datos";
    //Specification of the SQL query
    $query='SELECT * FROM historiaacademica where Escuela = "TEC " ';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);
    
    
    $table = '<table class="striped">
        <thead>
          <tr>
              <th>Idinfo</th>
              <th>IdBecario</th>
              <th>Escuela</th>
              <th>Grado</th>
              <th>Nivel</th>

          </tr>
        </thead>
        <tbody>';
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
        
    	$table .= '
    	<tr>
            <td>'.$fila["Idinfo"].'</td>
            <td>'.$fila["IdBecario"].'</td>
            <td>'.$fila["Escuela"].'</td>
            <td>'.$fila["Grado"].'</td>
            <td>'.$fila["Nivel"].'</td>

        </tr>';
      
    }
    // it releases the associated results
    mysqli_free_result($registros);
    
    desconectar($db);
    
    $table .= "</tbody></table>";
    
    return $table;
}



  // if(isset($_POST['boton']))
   // {
     //   GetConsultaBoton();
        
        
//    }
//


?>