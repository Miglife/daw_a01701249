-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-10-2017 a las 22:48:24
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `petz`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `IdEstado` int(11) NOT NULL,
  `EstadoActual` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`IdEstado`, `EstadoActual`) VALUES
(1, 'infeccion'),
(2, 'coma'),
(3, 'transformacion'),
(4, 'completamente muerto');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registro`
--

CREATE TABLE `registro` (
  `IdRegistro` int(11) NOT NULL,
  `Nombre` varchar(20) NOT NULL,
  `ApellidoP` varchar(20) DEFAULT NULL,
  `ApellidoM` varchar(20) DEFAULT NULL,
  `IdEstado` int(11) NOT NULL,
  `FechaHoraRegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `FechaHoraTransicion` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `registro`
--

INSERT INTO `registro` (`IdRegistro`, `Nombre`, `ApellidoP`, `ApellidoM`, `IdEstado`, `FechaHoraRegistro`, `FechaHoraTransicion`) VALUES
(1, 'Jose', 'Hernandez', 'Torres', 1, '2017-10-27 18:28:21', '2017-10-27 18:28:21'),
(2, 'Alfonso', 'Garza', 'Peña', 2, '2017-10-27 18:28:21', '2017-10-27 18:28:21'),
(3, 'Salvador', 'Cabañas', 'Bala', 3, '2017-10-27 18:29:13', '2017-10-27 18:29:13'),
(4, 'Ian', 'Treviño', 'Martinez', 4, '2017-10-27 18:29:13', '2017-10-27 18:29:13'),
(5, 'Diego', 'Nuñez', 'Alvarez', 1, '2017-10-27 18:30:22', '2017-10-27 18:30:22'),
(6, 'Nicolas', 'Inmaduro', 'Presidente', 4, '2017-10-27 18:30:22', '2017-10-27 18:30:22'),
(9, 'NoGuarda', 'Con', 'Boton', 1, '2017-10-27 20:01:27', '2017-10-27 20:01:27'),
(10, 'aaaaaaaaa', 'whyyy', 'noooooo', 2, '2017-10-27 20:01:27', '2017-10-27 20:01:27'),
(11, 'Rip', 'a este', 'examen', 4, '2017-10-27 20:02:12', '2017-10-27 20:02:12'),
(12, 'akdal', 'skdald', 'skals', 2, '2017-10-27 20:02:12', '2017-10-27 20:02:12'),
(14, 'adjskl', 'asjklda', 'asjdlk', 1, '2017-10-27 20:09:33', '0000-00-00 00:00:00'),
(15, 'Javier', 'Solis', 'Magaña', 3, '2017-10-27 20:13:19', '0000-00-00 00:00:00'),
(16, 'sadk', 'djask', 'aaaa', 2, '2017-10-27 20:44:38', '0000-00-00 00:00:00'),
(17, 'sadk', 'djask', 'aaaa', 2, '2017-10-27 20:44:43', '0000-00-00 00:00:00'),
(18, 'abc', 'dcf', 'aaaa', 2, '2017-10-27 20:44:56', '0000-00-00 00:00:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`IdEstado`);

--
-- Indices de la tabla `registro`
--
ALTER TABLE `registro`
  ADD PRIMARY KEY (`IdRegistro`),
  ADD KEY `EstadoActual` (`IdEstado`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `estado`
--
ALTER TABLE `estado`
  MODIFY `IdEstado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `registro`
--
ALTER TABLE `registro`
  MODIFY `IdRegistro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `registro`
--
ALTER TABLE `registro`
  ADD CONSTRAINT `registro_ibfk_1` FOREIGN KEY (`IdEstado`) REFERENCES `estado` (`IdEstado`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
