<?php

function conectar() {
    $mysql = mysqli_connect("localhost","root","","petz");
    $mysql->set_charset("utf8");
    return $mysql;
}

function desconectar($mysql) {
    mysqli_close($mysql);
}

function getTabla($tabla) {
    $db = conectar();
    
    //Specification of the SQL query
    $query='SELECT * FROM '.$tabla;
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
        for($i=0; $i<count($fila); $i++) {
            // use of numeric index
            echo $fila[$i].' '; 
        }
        echo '<br>';
    }
    echo '<br>';
    // it releases the associated results
    mysqli_free_result($registros);
    
    desconectar($db);
}

function getZombie($db, $IdEstado){
    //Specification of the SQL query
    $query='SELECT EstadoActual FROM estado WHERE IdEstado="'.$IdEstado.'"';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);   
    $fila = mysqli_fetch_array($registros, MYSQLI_BOTH);
    $zombie = $fila["EstadoActual"];
    return $zombie;
} // ya



function getRegistro($db, $registroId){
    //Specification of the SQL query
    $query='SELECT * FROM registro WHERE IdEstado="'.$registroId.'"';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);   
    $fila = mysqli_fetch_array($registros, MYSQLI_BOTH);
    return $fila;
}// pendiente



function getZombiesCards() {  //segunda CONSULTA
    $db = conectar();
    
    //Specification of the SQL query
    $query='SELECT * FROM registro ORDER BY FechaHoraRegistro desc';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);
    
    echo "<br> <br> <br>";
    echo "<br> <br> <br>";

    echo "La segunda consulta es: ";
    echo "<br> <br> <br>";
    
    $cards = "";
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
                                            // FALTA LO DE ID en editar               

    	$cards .= '
    	<div class="row">
        <div class="col s12 m6">
          <div class="card blue-grey darken-1">
            <div class="card-content white-text">
              <span class="card-title">'.$fila["Nombre"].' ('.getZombie($db, $fila["IdEstado"]).')</span>
              <p>'.$fila["ApellidoP"].'</p>
              <p>'.$fila["ApellidoM"].'</p>
              <br><br>
              <p>Fecha de registro:  '.$fila["FechaHoraRegistro"].'</p>
              <p>Fecha de Transicion:  '.$fila["FechaHoraTransicion"].'</p>

            </div>
            <a href="editar.php?IdEstado='.$fila["IdEstado"].'">Editar</a> 
            <div class="card-action">



            </div>
          </div>
        </div>
      </div>';
      
    }
    // it releases the associated results
    mysqli_free_result($registros);
    
    desconectar($db);
    
    return $cards;
}




function getConsulta1 (){
    
    $db = conectar();
    
    //Specification of the SQL query
    $query='SELECT count(*) from registro';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);
    
  //  $count = mysql_fetch_array($registros);

        $count = mysqli_fetch_array($registros, MYSQLI_BOTH);

    

       mysqli_free_result($registros);
    
    echo "La consulta 1 es: ";
    echo "<br>";
    echo "El numero de registros actuales son: ";
    echo "$count[0]";
    
    echo "<br>";
    echo "Los Ids correctos son 1,2,3,4 a falta de tiempo lo dejé así... salía valor nulo cuando usaba el getdropdown";
    echo "<br> 1 = infeccion <br>
    2 = coma <br>
    3 = transformacion <br>
    4 = completamente muerto <br> ";    
 
    
    
    
    
  $query='SELECT count(IdEstado) from registro where IdEstado=1';
    $registros = $db->query($query);
        $count2 = mysqli_fetch_array($registros, MYSQLI_BOTH);
    echo "<br>";
    echo "El numero en estado 1 son: ";
    echo "$count2[0]";
    
    $query='SELECT count(IdEstado) from registro where IdEstado=2';
    $registros = $db->query($query);
        $count3 = mysqli_fetch_array($registros, MYSQLI_BOTH);
    echo "<br>";
    echo "El numero en estado 2 son: ";
    echo "$count3[0]";
    
    $query='SELECT count(IdEstado) from registro where IdEstado=3';
    $registros = $db->query($query);
        $count4 = mysqli_fetch_array($registros, MYSQLI_BOTH);
    echo "<br>";
    echo "El numero en estado 3 son: ";
    echo "$count4[0]";
    
    $query='SELECT count(IdEstado) from registro where IdEstado=4';
    $registros = $db->query($query);
        $count5 = mysqli_fetch_array($registros, MYSQLI_BOTH);
    echo "<br>";
    echo "El numero en estado 4 son: ";
    echo "$count5[0]";
    
    
    $query='SELECT count(IdEstado) from registro where IdEstado!= 4';
    $registros = $db->query($query);
        $count6 = mysqli_fetch_array($registros, MYSQLI_BOTH);
    echo "<br>";
    echo "El numero de los que no estan completamente muertos son: ";
    echo "$count6[0]";
    
    
    
    
    desconectar($db);

}















function getDropdownZombie($tabla, $order_by_field, $selected=-1) {
     $db = conectar();
    
    //Specification of the SQL query
    $query='SELECT * FROM '.$tabla.' ORDER BY '.$order_by_field;
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);
    
    $select = '<div class="input-field col s12">
                <select name="'.$tabla.'_id">';
      
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
        if ($selected == $fila["IdEstado"]) {
            $select .= '<option value="'.$fila["IdEstado"].'" selected>'.$fila["$order_by_field"].'</option>';
        } else {
            $select .= '<option value="'.$fila["IdEstado"].'">'.$fila["$order_by_field"].'</option>';
        }
        
    }
    $select .= '</select><label for="'.$tabla.'">'.$tabla.'</label></div>';
    // it releases the associated results
    mysqli_free_result($registros);
    
    desconectar($db);
    
    return $select;
}

//deberia funcionar

function guardarRegistro($nombre, $IdEstado, $ApellidoP, $ApellidoM){
    $db = conectar();
    
    // insert command specification 
    $query='INSERT INTO registro (`nombre`, `ApellidoP` ,`ApellidoM`,`IdEstado`) VALUES (?,?,?,?) ';
    // Preparing the statement 
    if (!($statement = $db->prepare($query))) {
        die("Preparation failed: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params 
    if (!$statement->bind_param("ssss", $nombre, $ApellidoP, $ApellidoM, $IdEstado)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
    }
    // Executing the statement
    if (!$statement->execute()) {
        die("Execution failed: (" . $statement->errno . ") " . $statement->error);
     } 

    
    desconectar($db);
}

//------------------------------------------------------------


// solo el estado
/*
function editarRegistro($nombre, $IdEstado, $ApellidoP, $ApellidoM ){
    $db = conectar();
    
    // insert command specification 
    $query='UPDATE registro SET IdEstado=?, ApellidoP=?, ApellidoM=?, Nombre=? WHERE IdEstado=?';
    // Preparing the statement 
    if (!($statement = $db->prepare($query))) {
        die("Preparation failed: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params 
    if (!$statement->bind_param("ssss", $nombre, $ApellidoP, $ApellidoM, $IdEstado)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
    }
    // update execution
    if ($statement->execute()) {
        echo 'There were ' . mysqli_affected_rows($mysql) . ' affected rows';
    } else {
        die("Update failed: (" . $statement->errno . ") " . $statement->error);
    }
 

    
    desconectar($db);
}
*/
?>