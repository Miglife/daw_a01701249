<?php
    
    session_start();
    
    
$_SESSION["nombre"] = $_POST["nombre"];
$_SESSION["apellidop"] = $_POST["apellidop"];
$_SESSION["apellidom"] = $_POST["apellidom"];
$_SESSION["usuario"] = $_POST["usuario"];
$_SESSION["contrasena"] = $_POST["contrasena"]; 
$_SESSION["BooleanAValidacion"] = false;


    $BooleanApellidoP = false;
    $BooleanApellidoM = false;
    $BooleanNombre = false;
    $BooleanUsuario = false;



if ( strlen($_POST["nombre"])< 3  || (strlen($_POST["nombre"]) >10) ){
    // el nombre es menor a tres o mayor de 10 letras, 
    $_SESSION["error_nombre"] = '<span class="red">\nEl nombre debe contener más de 3 caracteres y menos de 10 caracteres.</span>'; 
    $_SESSION["nombre"] = $_POST["nombre"];
    header("location: index.php");
    
} else { 
        $BooleanNombre = true;

}


// fin validacion nombre


// los apellidos son menores a tres o mayores de 10 letras

if ( strlen($_POST["apellidop"])< 3  || (strlen($_POST["apellidop"]) >10) ){
    // el apellido es menor a tres o mayor de 10 letras, 
    $_SESSION["error_apellidop"] = '<span class="red">\nEl apellido debe contener más de 3 caracteres y menos de 10 caracteres.</span>'; 
    $_SESSION["apellidop"] = $_POST["apellidop"];
    header("location: index.php");
    
} else {
        $BooleanApellidoP = true;
}

if ( strlen($_POST["apellidom"])< 3 || (strlen($_POST["apellidom"]) >10) ){
    // el apellido es menor a tres o mayor de 10 letras, 
    $_SESSION["error_apellidom"] = '<span class="red">\nEl apellido debe contener más de 3 caracteres y menos de 10 caracteres.</span>'; 
    $_SESSION["apellidom"] = $_POST["apellidom"];
    header("location: index.php");
    
} else {
        $BooleanApellidoM = true;

}
// fin de validaciones de apellidos


// el usuario debe ser menor a tres o mayor de 10 letras
if ( strlen($_POST["usuario"])< 3  || (strlen($_POST["usuario"]) >10) ){
    // el apellido es menor a tres o mayor de 10 letras, 
    $_SESSION["error_usuario"] = '<span class="red">\nEl usuario debe contener más de 3 caracteres y menos de 10 caracteres.</span>'; 
    $_SESSION["usuario"] = $_POST["usuario"];
    header("location: index.php");
    
} else{
        $BooleanUsuario = true;

}



// que cumplan con los requisitos para poder pasar de pagina -> validacion correcta.
if( ($BooleanNombre= true) && ($BooleanApellidoP = true) && ($BooleanApellidoM = true) && ($BooleanUsuario = true) && (isset($_POST["contrasena"]))) {
    
        $target_dir = "media/";
        $target_file = $target_dir . basename($_FILES["foto"]["name"]);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["foto"]["tmp_name"]);
            if($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        }
        // Check if file already exists
        if (file_exists($target_file)) {
            echo "";  // ya existe en la carpeta media
            $uploadOk = 0;
        }
        // Check file size
        if ($_FILES["foto"]["size"] > 500000) {
            echo "La imagen es muy grande!";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            echo "Lo sentimos, solo archivos JPG, JPEG, PNG & GIF son aceptados.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Lo sentimos, no se pudo cargar tu imagen.";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["foto"]["tmp_name"], $target_file)) {
                echo "La imagen fue cargada exitosamente";
            } else {
                echo "Lo sentimos, no se pudo cargar tu imagen.";
            }
        }
        
        $_SESSION["foto"] = $target_file;
    
    
    
    
            $_SESSION["BooleanAValidacion"] = true; //para hacer validacion en validacioncorrecta.html
            include ("_encabezado.html");
            include ("validacioncorrecta.html");
            include ("_piedepagina.html");
            

} 

    
?>