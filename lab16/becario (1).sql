-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-10-2017 a las 02:53:11
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `becario`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `becados`
--

CREATE TABLE `becados` (
  `IdBecario` int(10) NOT NULL,
  `Nombre` text NOT NULL,
  `ApellidoP` text NOT NULL,
  `ApellidoM` text NOT NULL,
  `FechaNacimiento` date NOT NULL,
  `Email` varchar(25) NOT NULL,
  `Status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `becados`
--

INSERT INTO `becados` (`IdBecario`, `Nombre`, `ApellidoP`, `ApellidoM`, `FechaNacimiento`, `Email`, `Status`) VALUES
(1, 'Juan', 'Aboytes', 'Novoa', '1997-02-05', 'ochoa_aboytes@hotmail.com', 'Sin beca'),
(2, 'Juan', 'Perez', 'Juarez', '2017-10-05', 'lulu@hotmail.com', 'Condicionado'),
(3, 'Antonio', 'Candreva', 'Nose', '2017-10-01', 'inter@hotmail.com', 'Con beca');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historiaacademica`
--

CREATE TABLE `historiaacademica` (
  `Idinfo` int(10) NOT NULL,
  `IdBecario` int(10) NOT NULL,
  `Escuela` text NOT NULL,
  `Grado` int(11) NOT NULL,
  `Nivel` text NOT NULL,
  `Promedio` int(11) NOT NULL,
  `Estatus` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `historiaacademica`
--

INSERT INTO `historiaacademica` (`Idinfo`, `IdBecario`, `Escuela`, `Grado`, `Nivel`, `Promedio`, `Estatus`) VALUES
(11, 3, 'TEC', 1, 'Bachillerato', 90, 'con beca'),
(12, 2, 'UNAM', 2, 'Universidad', 80, 'condicionado');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `becados`
--
ALTER TABLE `becados`
  ADD PRIMARY KEY (`IdBecario`) USING BTREE;

--
-- Indices de la tabla `historiaacademica`
--
ALTER TABLE `historiaacademica`
  ADD PRIMARY KEY (`Idinfo`),
  ADD KEY `IdBecario` (`IdBecario`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `historiaacademica`
--
ALTER TABLE `historiaacademica`
  ADD CONSTRAINT `historiaacademica_ibfk_1` FOREIGN KEY (`IdBecario`) REFERENCES `becados` (`IdBecario`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
