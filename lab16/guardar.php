<?php
    session_start();
    require_once("ejercicio.php");
    if(isset($_POST["id"])) {
        editarRegistro($_POST["IdBecario"], $_POST["nombre"], $_POST["ApellidoP"], $_POST["ApellidoM"], $_POST["FechaN"], $_POST["Email"], $_POST["Status"]);
        $_SESSION["mensaje"] = $_POST["nombre"].' se actualizó correctamente';
    } else {
        guardarRegistro($_POST["IdBecario"], $_POST["nombre"], $_POST["ApellidoP"], $_POST["ApellidoM"], $_POST["FechaN"], $_POST["Email"], $_POST["Status"]);
        $_SESSION["mensaje"] = $_POST["nombre"].' se registró correctamente';
    }
    header("location:index.php");
?>