<?php

function conectar() {
    $mysql = mysqli_connect("localhost","root","","becario");
    $mysql->set_charset("utf8");
    return $mysql;
}

function desconectar($mysql) {
    mysqli_close($mysql);
}

// AQUI PROBANDO Weas

/*
function GetConsultaBoton(){
    
    $db= conectar();
    $nombre = $_POST['nombre'];

    //tiene que recibir el post 
    
    $query="SELECT * FROM becados WHERE nombre = $nombre";

    $registros = $db->query($query);

    
    
         
    $table = '<table class="striped">
        <thead>
          <tr>
              <th>Nombre</th>
              <th>Apellido Paterno</th>
              <th>Apellido Materno</th>
              <th>Status</th>
          </tr>
        </thead>
        <tbody>';
    
    $resultados = mysqli_query($conexion,"SELECT * FROM becados WHERE nombre = $nombre");
     while($consulta = mysqli_fetch_array($resultados))
          {
            	$table .= '
    	<tr>
            <td>'.$consulta["Nombre"].'</td>
            <td>'.$consulta["ApellidoP"].'</td>
            <td>'.$consulta["ApellidoM"].'</td>
            <td>'.$consulta["Status"].'</td>
        </tr>';
      
    }
    // it releases the associated results
    mysqli_free_result($registros);
    
    desconectar($db);
    
    $table .= "</tbody></table>";
    
    return $table;
    
    
    
}
*/





function Gettablasbecarios() {
    $db = conectar();
    
    //Specification of the SQL query
    $query='SELECT * FROM becados';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);
        
    echo "<br><br>";
    echo "Esta tabla representa todos los registros en la tabla becarios";

    
    $table = '<table class="striped">
        <thead>
          <tr>
              <th>Nombre</th>
              <th>Apellido Paterno</th>
              <th>Apellido Materno</th>
              <th>Status</th>
          </tr>
        </thead>
        <tbody>';
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
        
    	$table .= '
    	<tr>
            <td>'.$fila["Nombre"].'</td>
            <td>'.$fila["ApellidoP"].'</td>
            <td>'.$fila["ApellidoM"].'</td>
            <td>'.$fila["Status"].'</td>
        </tr>';
      
    }
    // it releases the associated results
    mysqli_free_result($registros);
    
    desconectar($db);
    
    $table .= "</tbody></table>";
    
    return $table;
}


function GettablaAntonio() {
    $db = conectar();
    
    echo "<br> <br>";
    echo "Esta tabla representa todos los datos del registro buscando 'Antonio' en la base de datos";
    //Specification of the SQL query
    $query='SELECT * FROM becados where nombre = "Antonio " ';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);
    
    
    $table = '<table class="striped">
        <thead>
          <tr>
              <th>Nombre</th>
              <th>Apellido Paterno</th>
              <th>Apellido Materno</th>
              <th>Status</th>
          </tr>
        </thead>
        <tbody>';
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
        
    	$table .= '
    	<tr>
            <td>'.$fila["Nombre"].'</td>
            <td>'.$fila["ApellidoP"].'</td>
            <td>'.$fila["ApellidoM"].'</td>
            <td>'.$fila["Status"].'</td>
        </tr>';
      
    }
    // it releases the associated results
    mysqli_free_result($registros);
    
    desconectar($db);
    
    $table .= "</tbody></table>";
    
    return $table;
}


function GettablaHistoriaA() {
    $db = conectar();
    
    echo "<br> <br>";
    echo "Esta tabla representa todos los datos de historiaacademica en la base de datos";
    //Specification of the SQL query
    $query='SELECT * FROM historiaacademica';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);
    
    
    $table = '<table class="striped">
        <thead>
          <tr>
              <th>Idinfo</th>
              <th>IdBecario</th>
              <th>Escuela</th>
              <th>Grado</th>
              <th>Nivel</th>
              <th>Promedio</th>
              <th>Estatus</th>
              
          </tr>
        </thead>
        <tbody>';
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
        
    	$table .= '
    	<tr>
            <td>'.$fila["Idinfo"].'</td>
            <td>'.$fila["IdBecario"].'</td>
            <td>'.$fila["Escuela"].'</td>
            <td>'.$fila["Grado"].'</td>
            <td>'.$fila["Nivel"].'</td>
            <td>'.$fila["Promedio"].'</td>
            <td>'.$fila["Estatus"].'</td>


        </tr>';
      
    }
    // it releases the associated results
    mysqli_free_result($registros);
    
    desconectar($db);
    
    $table .= "</tbody></table>";
    
    return $table;
}

function getBecariosCards() {
    $db = conectar();
    
    //Specification of the SQL query
    $query='SELECT * FROM becados';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);
    
    
    $cards = "";
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
        
    	$cards .= '
    	<div class="row">
        <div class="col s12 m6">
          <div class="card blue-grey darken-1">
            <div class="card-content white-text">
              <span class="card-title">'.$fila["IdBecario"].'</span>
              <p>'.$fila["Nombre"].'</p>
              <br>
              <p>'.$fila["ApellidoP"].'</p>
              <br>
              <p>'.$fila["ApellidoM"].'</p>
              <br>
              <p>'.$fila["FechaNacimiento"].'</p>
              <br>
              <p>'.$fila["Email"].'</p>
              <br>
              <p>'.$fila["Status"].'</p>
              <br>
            </div>
            <div class="card-action">
            
             <a href="editar.php?id='.$fila["IdBecario"].'">'.Editar.'</a>
             <a href="eliminar.php?id='.$fila["IdBecario"].'">'.Eliminar.'</a>

            </div>
          </div>
        </div>
      </div>';
      
    }
    // it releases the associated results
    mysqli_free_result($registros);
    
    desconectar($db);
    
    return $cards;
}








function GettablaHistoriaATEC() {
    $db = conectar();
    
    echo "<br> <br>";
    echo "Esta tabla representa algunas columnas del registro buscando 'TEC' en la base de datos";
    //Specification of the SQL query
    $query='SELECT * FROM historiaacademica where Escuela = "TEC " ';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);
    
    
    $table = '<table class="striped">
        <thead>
          <tr>
              <th>Idinfo</th>
              <th>IdBecario</th>
              <th>Escuela</th>
              <th>Grado</th>
              <th>Nivel</th>

          </tr>
        </thead>
        <tbody>';
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
        
    	$table .= '
    	<tr>
            <td>'.$fila["Idinfo"].'</td>
            <td>'.$fila["IdBecario"].'</td>
            <td>'.$fila["Escuela"].'</td>
            <td>'.$fila["Grado"].'</td>
            <td>'.$fila["Nivel"].'</td>

        </tr>';
      
    }
    // it releases the associated results
    mysqli_free_result($registros);
    
    desconectar($db);
    
    $table .= "</tbody></table>";
    
    return $table;
}


function guardarRegistro($IdBecario, $nombre, $ApellidoP, $ApellidoM, $FechaN, $Email, $Status){
    $db = conectar();
    
    //$Estatus = "Con beca";
    
   /* echo $IdBecario;
    echo "/n";
    echo $nombre;
    echo "/n";    
        echo $ApellidoP;
    echo "/n";
        echo $ApellidoM;
    echo "/n";
        echo $FechaN;
    echo "/n";
        echo $Email;
    echo "/n";
        echo $Status;
    echo "/n";*/
   // $strlen($_POST["apellidop"]
    
    // insert command specification 
    $query='INSERT INTO becados (`IdBecario`, `nombre` ,`ApellidoP`,`ApellidoM`,`FechaNacimiento`,`Email`,`Status` ) VALUES (?,?,?,?,?,?,?) ';
    // Preparing the statement 
    if (!($statement = $db->prepare($query))) {
        die("Preparation failed: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params 
    if (!$statement->bind_param("sssssss", $IdBecario, $nombre, $ApellidoP, $ApellidoM, $FechaN, $Email, $Status)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
    }
    // Executing the statement
    if (!$statement->execute()) {
        die("Execution failed: (" . $statement->errno . ") " . $statement->error);
     } 

    
    desconectar($db);
}

function editarRegistro($id, $IdBecario, $nombre, $ApellidoP, $ApellidoM, $FechaN, $Email, $Status){
    $db = conectar();
    
    // insert command specification  CHECAR LO DE ID 
    $query='UPDATE becados SET IdBecario=?, nombre=?, ApellidoP=?, ApellidoM=?, $FechaN=?, Email=?, Status=? WHERE id=?';
    // Preparing the statement 
    if (!($statement = $db->prepare($query))) {
        die("Preparation failed: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params 
    if (!$statement->bind_param("sssssss", $IdBecario, $nombre, $ApellidoP, $ApellidoM, $FechaN, $Email, $Status, $id)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
    }
    // update execution
    if ($statement->execute()) {
        echo 'There were ' . mysqli_affected_rows($mysql) . ' affected rows';
    } else {
        die("Update failed: (" . $statement->errno . ") " . $statement->error);
    }
    
    desconectar($db);
}


function deleteEntry($id_user){

    $conn = connectDb();

        // Deletion query construction

    $query= "DELETE FROM becados WHERE id_user=?";

    if (!($statement = $conn->prepare($query))) {
        die("The preparation failed: (" . $conn->errno . ") " . $conn->error. '     Valores incorrectos');
   }
    // Binding statement params
    if (!$statement->bind_param("s", $id_user)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error. '     Valores incorrectos');
 } 
    // delete execution
    if ($statement->execute()) {
        echo 'There were ' . mysqli_affected_rows($conn) . ' affected rows';
    } else {
        die("Update failed: (" . $statement->errno . ") " . $statement->error. '     Valores incorrectos');
    }
}

/*
function ValidaGuardarRegistro () {
    
    $IdBecario = $_POST ['IdBecario'];
    $name = $_POST ['name'];
    $ApellidoP = $_POST ['ApellidoP'];
    $ApellidoM = $_POST ['ApellidoM'];
    $FechaN = $_POST ['FechaN'];
    $Email = $_POST ['Email'];
    $Status = $_POST ['Status'];
    
    if (insertFruit($IdBecario, $name, $ApellidoP, $ApellidoM, $FechaN, $Email, $Status)){
        
        echo "Se inserto el registro con éxito";
        
    } else {
         echo "No se inserto nada mi chavo";
    }
    
    
    
    
    
}
*/

  // if(isset($_POST['boton']))
   // {
     //   GetConsultaBoton();
        
        
//    }
//


?>